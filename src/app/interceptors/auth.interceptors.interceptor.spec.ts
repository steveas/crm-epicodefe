import { TestBed } from '@angular/core/testing';

import { Auth.InterceptorsInterceptor } from './auth.interceptors.interceptor';

describe('Auth.InterceptorsInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      Auth.InterceptorsInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: Auth.InterceptorsInterceptor = TestBed.inject(Auth.InterceptorsInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
