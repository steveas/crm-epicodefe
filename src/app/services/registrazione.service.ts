import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {User} from '../classes/user'

@Injectable({
  providedIn: 'root'
})
export class RegistrazioneService {

  constructor(
    private http: HttpClient
  ) { }

  createUser(user: User){
    return this.http.post(environment.APIURL + 'api/auth/signup', user);
  }
}
