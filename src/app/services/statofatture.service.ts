import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Stato} from '../classes/stato';
import { stato } from '../interfaces/ifatture';

@Injectable({
  providedIn: 'root'
})
export class StatofattureService {

  constructor(
    private http: HttpClient
  ) { }

  getAllStatiFatture(){
    return this.http.get(environment.APIURL + 'api/statifattura?page=0&size=2&sort=id,ASC')
  }
}


