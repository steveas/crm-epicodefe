import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { IClienti } from '../interfaces/clienti';
import { Clienti} from '../classes/clienti';

@Injectable({
  providedIn: 'root'
})
export class ClientiService {

  index:number = 0;

  constructor(
    private http: HttpClient
  ) { }

  getAllClients(){
    return this.http.get(environment.APIURL + 'api/clienti?page=0&size=2000&sort=id,ASC')
  }

  getPagedUsers(page: number){  //page: number
    return this.http.get(environment.APIURL+`api/clienti?page=${page}&size=5&sort=id,ASC`)   
  }

  deleteUser(user: IClienti){
    return this.http.delete(environment.APIURL + 'api/clienti/' +user.id)
  }

  createUser(user: Clienti){
    return this.http.post(environment.APIURL + 'api/clienti/', user);
  }

  getClientibyID(user : IClienti){
    return this.http.get<IClienti>(environment.APIURL + 'api/clienti/' +  user.id);
  }

  updateClienti(user: IClienti){
    return this.http.put(environment.APIURL +'api/clienti/' + user.id, user);
  }

}
