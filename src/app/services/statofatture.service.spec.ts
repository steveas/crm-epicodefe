import { TestBed } from '@angular/core/testing';

import { StatofattureService } from './statofatture.service';

describe('StatofattureService', () => {
  let service: StatofattureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatofattureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
