import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {provincia} from '../interfaces/clienti';
import {Provincia} from '../classes/provincia';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {

  constructor(
    private http: HttpClient
  ) { }

  getAllProvince(){
    return this.http.get(environment.APIURL + 'api/province?page=0&size=200&sort=id,ASC')
  }

  createProvince(user: provincia){
    return this.http.post<provincia>(environment.APIURL + 'api/province', user);
  }

  deleteProvince(user: provincia){
    return this.http.delete(environment.APIURL + 'api/province/' +user.id)
  }

  updateProvincia(user: provincia){
    return this.http.put(environment.APIURL +'api/province/' + user.id, user);
  }
}
