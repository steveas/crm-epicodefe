import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {comune, provincia} from '../interfaces/clienti';
import { Comune} from '../classes/comune';

@Injectable({
  providedIn: 'root'
})
export class ComuniService {

  constructor(
    private http: HttpClient
  ) { }

  getAllComuni(){
    return this.http.get(environment.APIURL + 'api/comuni') // api/comuni?page=0&size=20&sort=id,ASC
    //console.log(this.comunidue) 
  }

  createComuni(user: comune){
    return this.http.post<comune>(environment.APIURL + 'api/comuni/', user);
  }

  updateComuni(user: Comune){
    return this.http.put(environment.APIURL +'api/comuni/' + user.id, user);
  }

  deleteComuni(user: comune){
    return this.http.delete(environment.APIURL + 'api/comuni/' +user.id)
  }
}
