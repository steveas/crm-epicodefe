import { Injectable } from '@angular/core';
import {Ifatture} from '../interfaces/ifatture';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {Clienti} from '../classes/clienti';

@Injectable({
  providedIn: 'root'
})
export class FattureService {

  constructor(
    private http: HttpClient
  ) { }

  getAllFatture(){
    return this.http.get<Ifatture[]>(environment.APIURL + 'api/fatture?page=0&size=5000&sort=id,ASC');
  }

  getPagedUsers(page: number){
    return this.http.get(environment.APIURL+`api/fatture?page=${page}&size=5&sort=id,ASC`) 
  }

  createUser(user: Ifatture){
    return this.http.post(environment.APIURL + 'api/fatture', user);
  }

  updateFatture(user: Ifatture){
    return this.http.put(environment.APIURL +'api/fatture/' + user.id, user);
  }

  deleteFatture(user: Ifatture){
    return this.http.delete(environment.APIURL + 'api/fatture/' +user.id)
  }

  deleteFattureByClienti(user: Clienti){
    return this.http.delete(environment.APIURL + 'api/fatture/cliente/' +user.id)
  }
}
