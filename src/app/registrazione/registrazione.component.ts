import { Component, OnInit } from '@angular/core';
import {User} from '../classes/user';
import { RegistrazioneService } from '../services/registrazione.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrazione',
  templateUrl: './registrazione.component.html',
  styleUrls: ['./registrazione.component.css']
})
export class RegistrazioneComponent implements OnInit {

  user: User= new User();

  constructor(
    private registrazioneService: RegistrazioneService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  subscribe(user: User){ 

    //validazione form
    if((this.user.nome == "") || (this.user.nome ==   undefined) ||
        (this.user.cognome == "") || (this.user.cognome ==  undefined ) ||
        (this.user.email == "") || (this.user.email == undefined ) || 
        (this.user.password ==  "") || (this.user.password == undefined ) ||
        (this.user.username ==  "") || (this.user.username == undefined )){ 
    
          return alert("Attenzione, inserire tutti i campi!")
    }
    
    this.registrazioneService.createUser(user).subscribe(elem => console.log(' operazione eseguita con successo!'));
    this.router.navigate(['']);
  }

}
