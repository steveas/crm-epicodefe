import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { ClientiService } from '../services/clienti.service';
import { FattureService } from '../services/fatture.service';
import { Clienti } from '../classes/clienti';
import { Fatture } from '../classes/fatture';

@Component({
  selector: 'app-initialpage',
  templateUrl: './initialpage.component.html',
  styleUrls: ['./initialpage.component.css']
})
export class InitialpageComponent implements OnInit {

  clienti: Clienti[]=[]
  fatture: Fatture[] = []

  constructor(
    private router: Router,
    private loginService: LoginService,
    private clientiService: ClientiService,
    private fattureService: FattureService
  ) { }

  ngOnInit(): void {
    
      //Ottengo tutti i clienti per vedere poi quanti sono
      this.clientiService.getAllClients().subscribe((users:any) => {
        
          this.clienti = users.content
          console.log(this.clienti.length)
 
      });
     
      //Ottengo tutte le fatture per vedere poi quante sono
      this.fattureService.getAllFatture().subscribe((users:any) => {
        
          this.fatture = users.content
          console.log(this.fatture.length)
 
      });
        
  }

  logout(){

    this.loginService.logout();
    this.router.navigate(['']);

  }

}
