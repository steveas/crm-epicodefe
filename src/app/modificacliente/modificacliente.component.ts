import { Component, OnInit, EventEmitter, Input,  Output} from '@angular/core';
import { Clienti } from '../classes/clienti';
import { ClientiService } from '../services/clienti.service';

@Component({
  selector: 'app-modificacliente',
  templateUrl: './modificacliente.component.html',
  styleUrls: ['./modificacliente.component.css']
})
export class ModificaclienteComponent implements OnInit {

  @Output() onEditUser = new EventEmitter();
  @Input() editingUser!: Clienti;
  user:Clienti = new Clienti();

  constructor(
    private clientiService: ClientiService
  ) { }

  ngOnInit(): void {

    this.user = this.editingUser

  }

  editCliente(user: Clienti): void {

    //Validazioni modifica cliente
    if((this.user.nomeContatto == "") || (this.user.nomeContatto == undefined) ||
        (this.user.cognomeContatto == "") || (this.user.cognomeContatto == undefined) ||
        (this.user.pec == "") || (this.user.pec == undefined) ||
        (this.user.email == "") || (this.user.email == undefined) ||
        (this.user.telefonoContatto == "") || (this.user.telefonoContatto == undefined) ||
        (this.user.fatturatoAnnuale == null) || (this.user.fatturatoAnnuale == undefined) ||
        (this.user.indirizzoSedeOperativa.via == "") || (this.user.indirizzoSedeOperativa.via == undefined) ||
        (this.user.indirizzoSedeOperativa.civico == null) || (this.user.indirizzoSedeOperativa.civico == undefined) ||
        (this.user.indirizzoSedeOperativa.localita == "") || (this.user.indirizzoSedeOperativa.localita == undefined) ||
        (this.user.indirizzoSedeOperativa.cap == null) || (this.user.indirizzoSedeOperativa.cap == undefined) ||
        (this.user.indirizzoSedeOperativa.comune.nome == "") || (this.user.indirizzoSedeOperativa.comune.nome == undefined) ||
        (this.user.indirizzoSedeLegale.via == "") || (this.user.indirizzoSedeLegale.via == undefined) ||
        (this.user.indirizzoSedeLegale.civico == null) || (this.user.indirizzoSedeLegale.civico == undefined) || 
        (this.user.indirizzoSedeLegale.localita == "") || (this.user.indirizzoSedeLegale.localita == undefined) ||
        (this.user.indirizzoSedeLegale.cap == null) || (this.user.indirizzoSedeLegale.localita == undefined) || 
        (this.user.indirizzoSedeLegale.comune.nome == "") || (this.user.indirizzoSedeLegale.localita == undefined)){

        return alert('Inserire tutti i campi!');
    }
    
    this.clientiService.updateClienti(user).subscribe(data => {

      this.onEditUser.emit(user)

    })
  }

  close(){

    this.onEditUser.emit()
    
  }

}
