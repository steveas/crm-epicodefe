import {Ifatture, stato} from '../interfaces/ifatture';
import {IClienti,  sede,comune,provincia} from '../interfaces/clienti';

export class Fatture implements Ifatture{

    id!: number;
    data!: string;
    numero!: number;
    anno!: number;
    importo!: number;
    stato!: stato;
    cliente!: IClienti


}
