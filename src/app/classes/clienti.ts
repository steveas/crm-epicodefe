import {IClienti, sede,comune,provincia } from '../interfaces/clienti';

export class Clienti implements IClienti{
    id!: number;
    ragioneSociale!: string;
    partitaIva!: number;
    tipoCliente!: string;
    email!: string;
    pec!: string;
    telefono!: string
    nomeContatto!: string;
    cognomeContatto!: string;
    telefonoContatto!: string;
    emailContatto!: string;
    indirizzoSedeOperativa!: sede;    
    indirizzoSedeLegale!: sede; 
    dataInserimento!: string;
    dataUltimoContatto!: string;
    fatturatoAnnuale!: number;
    }
    
    



