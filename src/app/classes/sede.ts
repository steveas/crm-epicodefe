import { sede, comune, provincia } from '../interfaces/clienti';
import { Comune } from '../classes/comune';

export class Sede implements sede{
    id!: number;
    via!: string;
    civico!: number;
    cap!: number;
    localita!: string;
    comune: comune= new Comune;

}
