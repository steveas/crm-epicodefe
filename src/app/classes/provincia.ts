import { provincia } from '../interfaces/clienti';

export class Provincia implements provincia{

    id!: number;
    nome!: string;
    sigla!: string;

}
