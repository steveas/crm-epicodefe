import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { AdminLogin } from '../classes/admin-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private loginService : LoginService
  ) { }
  
  payload: AdminLogin = new AdminLogin();

  ngOnInit(): void {
  }

  login(){

    this.loginService.login(this.payload).subscribe(data => {

      localStorage.setItem('accessToken',data.accessToken);
      localStorage.setItem('currentUser',JSON.stringify(data.id));
      this.loginService.currentUser = data; 
    })  
    
    this.router.navigate(['start'])

  }

}
