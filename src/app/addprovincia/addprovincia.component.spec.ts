import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddprovinciaComponent } from './addprovincia.component';

describe('AddprovinciaComponent', () => {
  let component: AddprovinciaComponent;
  let fixture: ComponentFixture<AddprovinciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddprovinciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddprovinciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
