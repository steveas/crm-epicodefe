import { Component, OnInit } from '@angular/core';
import { provincia} from '../interfaces/clienti';
import {Provincia} from '../classes/provincia'
import { ActivatedRoute, Router } from '@angular/router';
import {ProvinceService} from '../services/province.service';

@Component({
  selector: 'app-addprovincia',
  templateUrl: './addprovincia.component.html',
  styleUrls: ['./addprovincia.component.css']
})
export class AddprovinciaComponent implements OnInit {

  provincia: provincia= new Provincia();
  province: Provincia[]= [];

  openEdit:boolean = false;
  selectedUser!: Provincia;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private provinceService: ProvinceService,
  ) { }

  ngOnInit(): void {

        this.provinceService.getAllProvince().subscribe((users:any) => {
        
          this.province = users.content;
          console.log(this.province);

        });

  }

  delprovincia( provincia : Provincia){

    this.provinceService.deleteProvince( provincia ).subscribe((users:any) => {
     
      this.province = users.content;
    });

    console.log("fatto");
  }

  selectUser( provincia: Provincia){
    
    this.selectedUser = provincia;
    this.openEdit = true;

  }
  
  closePopup(){

  this.openEdit = false;

  } 

  addprovincia(user: provincia){
    
    //Validazioni Aggiunta comune
    if((user.nome == "") || (user.nome == undefined) ||
        (user.sigla == "") || (user.sigla == undefined)){

      return alert('Inserire tutti i campi!')
   }
   
    console.log(user)
    
    this.provinceService.createProvince(user).subscribe(elem => console.log(' operazione eseguita con successo!') )
    this.router.navigate(['clienti']);

  }

}




 

