import {IClienti} from './clienti';

export interface Ifatture {
    id: number;
    data: string;
    numero: number;
    anno: number;
    importo: number;
    stato: stato;
    cliente: IClienti
}

export interface stato{
    id: number;
    nome: string;
}

