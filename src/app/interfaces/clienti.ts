export interface IClienti {
    id: number;
    ragioneSociale: string;
    partitaIva: number;
    tipoCliente: string;
    email: string;
    pec: string;
    telefono: string
    nomeContatto: string;
    cognomeContatto: string;
    telefonoContatto: string;
    emailContatto: string;
    indirizzoSedeOperativa: sede;    
    indirizzoSedeLegale: sede; 
    dataInserimento: string;
    dataUltimoContatto: string;
    fatturatoAnnuale: number;
}
/*
export interface indirizzosedeoperativa{
    id: number;
    via: string;
    civico: number;
    cap: number;
    localita: string;
    comune: comune;
} // chiamala sede*/

export interface sede{
    id: number;
    via: string;
    civico: number;
    cap: number;
    localita: string;
    comune: comune;
} 


export interface comune{

    id: number;
    nome: string;
    provincia: provincia;
}

export interface provincia{

    id: number;
    nome: string;
    sigla: string;
}
