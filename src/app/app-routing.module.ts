import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddclienteComponent } from './addcliente/addcliente.component';
import { AddcomuneComponent } from './addcomune/addcomune.component';
import { AddfatturaComponent } from './addfattura/addfattura.component';
import { AddprovinciaComponent } from './addprovincia/addprovincia.component';
import { ClientiComponent } from './clienti/clienti.component';
import { FattureComponent } from './fatture/fatture.component';
import { ControlGuard } from './guards/control.guard';
import { LoginGuard } from './guards/login.guard';
import { InitialpageComponent } from './initialpage/initialpage.component';
import { LoginComponent } from './login/login.component';
import { RegistrazioneComponent } from './registrazione/registrazione.component';


const routes: Routes = [
  
  //Login
  { path: "", component: LoginComponent, canActivate: [LoginGuard]},

  //Registrazione degli user
  { path: "registrazione", component: RegistrazioneComponent},  

  //Dashboard, se l'utente è già loggato entra automaticamante qui
  { path: "start", component: InitialpageComponent, canActivate: [ControlGuard]},

  //Clienti --> visualizzo modifico elimino e aggiungo clienti
  { path: "clienti", component: ClientiComponent, canActivate: [ControlGuard]},
  { path:"clienti/page/:page", component: ClientiComponent, canActivate: [ControlGuard]},

  //Fatture --> visualizzo modifico e aggiungo fatture
  { path: "fatture", component: FattureComponent, canActivate: [ControlGuard]},
  { path: "fatture/page/:page", component: FattureComponent, canActivate: [ControlGuard]},

  //Addcliente --> aggiunge nell'api un cliente e lo fa poi visualizzare in clienti
  { path: "clienti/addcliente", component: AddclienteComponent, canActivate: [ControlGuard]},
  { path:"clienti/page/:page/addcliente", component: AddclienteComponent, canActivate: [ControlGuard]},

  //Addfattura --> aggiunge nell'api una fattura e lo fa poi visualizzare in fatture
  { path: "fatture/addfattura", component: AddfatturaComponent, canActivate: [ControlGuard]},
  { path: "fatture/page/:page/addfattura", component: AddfatturaComponent, canActivate: [ControlGuard]},
  
  //Addcomune --> aggiunge all'api comune un nuovo comune che si va a visualizzare nell'addcliente
  { path: "clienti/addcliente/addcomune", component: AddcomuneComponent, canActivate: [ControlGuard]},
  { path:"clienti/page/:page/addcliente/addcomune", component: AddcomuneComponent, canActivate: [ControlGuard]},

  //Addprovincia --> aggiunge all'api comune un nuovo comune che si va a visualizzare nell'addcliente
  { path: "clienti/addcliente/addcomune/addprovincia", component: AddprovinciaComponent, canActivate: [ControlGuard]},
  { path:"clienti/page/:page/addcliente/addcomune/addprovincia", component: AddprovinciaComponent, canActivate: [ControlGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
