import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificaprovinciaComponent } from './modificaprovincia.component';

describe('ModificaprovinciaComponent', () => {
  let component: ModificaprovinciaComponent;
  let fixture: ComponentFixture<ModificaprovinciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificaprovinciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificaprovinciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
