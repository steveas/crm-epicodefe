import { Component, OnInit, EventEmitter, Input,  Output } from '@angular/core';
import { ProvinceService } from '../services/province.service';
import { Provincia } from '../classes/provincia';

@Component({
  selector: 'app-modificaprovincia',
  templateUrl: './modificaprovincia.component.html',
  styleUrls: ['./modificaprovincia.component.css']
})
export class ModificaprovinciaComponent implements OnInit {

  @Output() onEditUser = new EventEmitter();
  @Input() editingUser!: Provincia;
  user:Provincia = new Provincia();

  constructor(
    private provinceService: ProvinceService
  ) { }

  ngOnInit(): void {

    this.user = this.editingUser

  }

  editProvincia(user: Provincia): void {

    //Validazioni modifica cliente
    if((this.user.nome == "") || (this.user.nome == undefined) ||
        (this.user.sigla == "") || (this.user.sigla == undefined)){
       return alert('Inserire tutti i campi!');

    }

    this.provinceService.updateProvincia(user).subscribe(data => {

      this.onEditUser.emit(user)

    })
  }

  close(){

    this.onEditUser.emit()
    
  }

}






