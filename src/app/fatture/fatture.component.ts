import { Component, OnInit } from '@angular/core';
import {Ifatture} from '../interfaces/ifatture';
import { FattureService } from '../services/fatture.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Fatture } from '../classes/fatture';

@Component({
  selector: 'app-fatture',
  templateUrl: './fatture.component.html',
  styleUrls: ['./fatture.component.css']
})
export class FattureComponent implements OnInit {

  fatture: Fatture[]=[];
  maxpag: number[]= [1,2,3,4,5,6,7,8,9,10];

  openEdit:boolean = false;
  selectedUser!: Fatture;

  constructor(
    private fattureService: FattureService,
    private router: Router, 
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      
      this.fattureService.getPagedUsers(params["page"]).subscribe((users:any) => {
       
        this.fatture = users.content
        
      });
    })
  }

  delfattura( fattura : Ifatture){

    this.fattureService.deleteFatture( fattura ).subscribe((users:any) => {
     
      this.fatture = users.content});
      console.log("fatto");

  }

  selectUser(user: Fatture){
    
    this.selectedUser = user;
    this.openEdit = true;

  }
  
  closePopup(){

  this.openEdit = false;
  
  }

}



