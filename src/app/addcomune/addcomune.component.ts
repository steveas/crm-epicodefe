import { Component, OnInit } from '@angular/core';
import {comune, provincia} from '../interfaces/clienti';
import {Comune} from '../classes/comune';
import { ComuniService } from '../services/comuni.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Provincia } from '../classes/provincia';
import {ProvinceService} from '../services/province.service';


@Component({
  selector: 'app-addcomune',
  templateUrl: './addcomune.component.html',
  styleUrls: ['./addcomune.component.css']
})
export class AddcomuneComponent implements OnInit {

  comune: comune= new Comune();
  comuni: Comune[]= []
  provincia: Provincia[]= []

  openEdit:boolean = false;
  selectedUser!: Comune;

  constructor(
    private router: Router,
    private comuniService: ComuniService,
    private route: ActivatedRoute,
    private provinceService: ProvinceService,
  ) { }

  ngOnInit(): void {
  
      this.comuniService.getAllComuni().subscribe((users:any) => {
        
         this.comuni = users.content
         console.log(this.comuni)  
      });

      this.provinceService.getAllProvince().subscribe((users:any) => {//passo il parametro page alla chiamata che mi restituirà gli utenti in versione paginata ///params["page"]
        
          this.provincia = users.content
          console.log(this.provincia)  
      });

  }

  delcomune( comune : comune){

    this.comuniService.deleteComuni( comune ).subscribe((users:any) => {
     
      this.comuni = users.content
    });

    console.log("fatto")
     
  }

  selectUser(user: Comune){
    
    this.selectedUser = user;
    this.openEdit = true;

  }
  
  closePopup(){

  this.openEdit = false;

  } 

  addcomune(user: comune){
     
    //Validazioni Aggiunta comune
    if((user.nome == "") || (user.nome == undefined)){
      return alert('Inserire nome del comune!')
   }
   
    console.log(user)
    
    this.comuniService.createComuni(user).subscribe(elem => console.log(' operazione eseguita con successo!') )
    this.router.navigate(['clienti']);

  }
  
}
