import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcomuneComponent } from './addcomune.component';

describe('AddcomuneComponent', () => {
  let component: AddcomuneComponent;
  let fixture: ComponentFixture<AddcomuneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddcomuneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcomuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
