import { Component, OnInit,EventEmitter, Input,  Output } from '@angular/core';
import { Fatture } from '../classes/fatture';
import { FattureService } from '../services/fatture.service';

@Component({
  selector: 'app-modificafattura',
  templateUrl: './modificafattura.component.html',
  styleUrls: ['./modificafattura.component.css']
})
export class ModificafatturaComponent implements OnInit {

  @Output() onEditUser = new EventEmitter();
  @Input() editingUser!: Fatture;
  user:Fatture = new Fatture();

  constructor(
    private fattureService: FattureService
  ) { }

  ngOnInit(): void {

    this.user = this.editingUser

  }

  editCliente(user: Fatture): void {

    //Validazioni modifica cliente
    if((this.user.cliente.nomeContatto == "") || (this.user.cliente.nomeContatto == undefined) ||
        (this.user.cliente.cognomeContatto == "") || (this.user.cliente.cognomeContatto == undefined) ||
        (this.user.importo == null) || (this.user.importo == undefined) ||
        (this.user.data == null) || (this.user.data == undefined) ||
        (this.user.numero == null) || (this.user.numero == undefined)){
          
       return alert('Inserire tutti i campi!')
    }

    this.fattureService.updateFatture(user).subscribe(data => {

      this.onEditUser.emit(user)

    })
  }

  close(){

    this.onEditUser.emit();

  }

}




