import { Component, OnInit,  } from '@angular/core';
import { ClientiService } from '../services/clienti.service';
import {IClienti} from '../interfaces/clienti';
import { Clienti } from '../classes/clienti';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FattureService } from '../services/fatture.service';

@Component({
  selector: 'app-clienti',
  templateUrl: './clienti.component.html',
  styleUrls: ['./clienti.component.css']
})
export class ClientiComponent implements OnInit{

  customers: Clienti[]= [];
  maxpag: number[]= [1,2,3,4,5,6,7,8,9,10];

  openEdit:boolean = false;
  selectedUser!: Clienti;
  
  constructor(
    private clientiService: ClientiService,
    private route: ActivatedRoute,
    private router: Router,
    private fattureService: FattureService,
  ) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      
    this.clientiService.getPagedUsers(params["page"]).subscribe((users:any) => {
       
        this.customers = users.content

      });
    })

  }

  delcliente( cliente : IClienti){

      //this.fattureService.deleteFattureByClienti(cliente).subscribe(elem => console.log('fatture del cliente eliminate'))
      this.clientiService.deleteUser( cliente ).subscribe((users:any) => {
       
        this.customers = users.content
        

      });
      console.log("fatto")

  }

  selectUser(user: Clienti){
      
      this.selectedUser = user;
      this.openEdit = true;

  }
  
  closePopup(){

    this.openEdit = false;
    
  }

}

  



      

  




