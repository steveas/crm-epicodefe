import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FattureService } from '../services/fatture.service';
import { Fatture } from '../classes/fatture';
import { Ifatture } from '../interfaces/ifatture';
import { Clienti } from '../classes/clienti';
import { IClienti } from '../interfaces/clienti';
import {ClientiService } from '../services/clienti.service';
import { Stato } from '../classes/stato';
import { StatofattureService } from '../services/statofatture.service';

@Component({
  selector: 'app-addfattura',
  templateUrl: './addfattura.component.html',
  styleUrls: ['./addfattura.component.css']
})
export class AddfatturaComponent implements OnInit {

  fattura: Fatture= new Fatture();
  customers: Clienti[]=[];
  statoF: Stato[]= []

  constructor(
    private router: Router,
    private fattureService: FattureService,
    private clientiService: ClientiService,
    private route: ActivatedRoute,
    private statofattureService: StatofattureService
  ) { }

  ngOnInit(): void {

      this.clientiService.getAllClients().subscribe((users:any) => {
        
         this.customers = users.content;
         console.log(this.customers)

      });

      this.statofattureService.getAllStatiFatture().subscribe((users:any) => {
        
        this.statoF = users.content;
        console.log(this.statoF);

      });
     
  }

  addFattura(user: Fatture){ 
    
  //Validazioni Aggiunta fattura
  if((user.numero == null) || (user.numero == undefined) ||
  (user.importo == null) || (user.numero == undefined) ||
  (user.data == "") || (user.data == undefined) ||
  (user.anno  == null) || (user.anno == undefined)){

    return alert('Inserire tutti i campi')
  }
 
    console.log(user)
    
    this.fattureService.createUser(user).subscribe(elem => console.log(' operazione eseguita con successo!') )
    this.router.navigate(['fatture']);
  }
}




