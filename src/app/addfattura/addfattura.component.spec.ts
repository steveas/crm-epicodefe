import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddfatturaComponent } from './addfattura.component';

describe('AddfatturaComponent', () => {
  let component: AddfatturaComponent;
  let fixture: ComponentFixture<AddfatturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddfatturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddfatturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
