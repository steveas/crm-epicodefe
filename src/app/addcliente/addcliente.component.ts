import { Component, OnInit } from '@angular/core';
import { ClientiService } from '../services/clienti.service';
import { Clienti } from '../classes/clienti';
import { IClienti } from '../interfaces/clienti';
import { Fatture } from '../classes/fatture';
import { Ifatture } from '../interfaces/ifatture';
import { ActivatedRoute, Router } from '@angular/router';
import { ComuniService } from '../services/comuni.service';
import {comune, provincia} from '../interfaces/clienti';
import { Comune} from '../classes/comune';
import {Sede } from '../classes/sede';

@Component({
  selector: 'app-addcliente',
  templateUrl: './addcliente.component.html',
  styleUrls: ['./addcliente.component.css']
})
export class AddclienteComponent implements OnInit {

  cliente: IClienti= new Clienti();
  SedeLegale: Sede=new Sede();
  SedeOperativa: Sede= new Sede();
  comuni: Comune[]= []
  comune!: Comune[];
  prova: any;

  constructor(
    private router: Router,
    private clientiService: ClientiService,
    private comuniService: ComuniService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void { 
      
      this.comuniService.getAllComuni().subscribe((users:any) => {
        
         this.comuni = users.content
         this.comune= this.comuni

         console.log(this.comuni)

       });
     
  }

  
  addCliente(user: Clienti){ 
    
  //Validazioni Aggiunta cliente
  if((user.nomeContatto == "") || (user.nomeContatto == undefined) ||
       (user.cognomeContatto == "") || (user.cognomeContatto == undefined) || 
       (user.telefonoContatto == "") || (user.telefonoContatto == undefined) ||
       (user.emailContatto == "") || (user.emailContatto == undefined) ||
       (user.telefono == "") || (user.telefono == undefined) ||
       (user.email == "") || (user.email == undefined) ||
       (user.pec == "") || (user.pec == undefined) ||
       (user.ragioneSociale == "") || (user.ragioneSociale == undefined) ||
       (user.partitaIva == null) || (user.partitaIva == undefined) ||
       (this.SedeOperativa.via == "") || (this.SedeOperativa.via == undefined) ||
       (this.SedeOperativa.civico == null) || (this.SedeOperativa.civico == undefined) ||
       (this.SedeOperativa.localita == "") || (this.SedeOperativa.localita == undefined) ||
       (this.SedeOperativa.cap == null) || (this.SedeOperativa.cap == undefined) ||
       (this.SedeLegale.via == "") || (this.SedeLegale.via == undefined) ||
       (this.SedeLegale.civico == null) || (this.SedeLegale.civico == undefined) ||
       (this.SedeLegale.localita == "") || (this.SedeLegale.localita == undefined) ||
       (this.SedeLegale.cap == null) || (this.SedeLegale.cap == undefined) || 
       (user.dataInserimento == "") || (user.dataInserimento == undefined) ||
       (user.dataUltimoContatto == "") || (user.dataUltimoContatto == undefined) ||
       (user.fatturatoAnnuale == null) || (user.fatturatoAnnuale == undefined)){
         
      return alert('Inserire tutti i campi!')
   }
   
    console.log(user)
    user.indirizzoSedeOperativa= this.SedeOperativa
    user.indirizzoSedeLegale= this.SedeLegale
    

    this.clientiService.createUser(user).subscribe(elem => console.log(' operazione eseguita con successo!') )
    this.router.navigate(['clienti']);
  }

}
