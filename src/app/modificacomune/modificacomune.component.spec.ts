import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacomuneComponent } from './modificacomune.component';

describe('ModificacomuneComponent', () => {
  let component: ModificacomuneComponent;
  let fixture: ComponentFixture<ModificacomuneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificacomuneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacomuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
