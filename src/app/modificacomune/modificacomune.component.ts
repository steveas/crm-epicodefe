import { Component, OnInit, EventEmitter, Input,  Output} from '@angular/core';
import { Comune } from '../classes/comune';
import { ComuniService } from '../services/comuni.service';

@Component({
  selector: 'app-modificacomune',
  templateUrl: './modificacomune.component.html',
  styleUrls: ['./modificacomune.component.css']
})
export class ModificacomuneComponent implements OnInit {

  @Output() onEditUser = new EventEmitter();
  @Input() editingUser!: Comune;
  user:Comune = new Comune();

  constructor(
    private comuniService: ComuniService
  ) { }

  ngOnInit(): void {

    this.user = this.editingUser

  }

  editCliente(user: Comune): void {

    //Validazioni modifica cliente
    if((this.user.nome == "") || (this.user.nome == undefined) ||
        (this.user.provincia.nome == "") || (this.user.provincia.nome == undefined) ||
        (this.user.provincia.sigla == "") || (this.user.provincia.sigla == undefined)){
       return alert('Inserire tutti i campi!');

    }

    this.comuniService.updateComuni(user).subscribe(data => {

      this.onEditUser.emit(user)

    })

  }

  close(){

    this.onEditUser.emit()
    
  }

}


