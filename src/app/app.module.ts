import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {  AuthInterceptorsInterceptor } from './interceptors/auth.interceptors.interceptor';
import { InitialpageComponent } from './initialpage/initialpage.component';
import { ClientiComponent } from './clienti/clienti.component';
import { FattureComponent } from './fatture/fatture.component';
import { RegistrazioneComponent } from './registrazione/registrazione.component';
import { AddclienteComponent } from './addcliente/addcliente.component';
import { AddfatturaComponent } from './addfattura/addfattura.component';
import { AddcomuneComponent } from './addcomune/addcomune.component';
import { ModificaclienteComponent } from './modificacliente/modificacliente.component';
import { ModificafatturaComponent } from './modificafattura/modificafattura.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { ModificacomuneComponent } from './modificacomune/modificacomune.component';
import { AddprovinciaComponent } from './addprovincia/addprovincia.component';
import { ModificaprovinciaComponent } from './modificaprovincia/modificaprovincia.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    InitialpageComponent,
    InitialpageComponent,
    ClientiComponent,
    FattureComponent,
    RegistrazioneComponent,
    AddclienteComponent,
    AddfatturaComponent,
    AddcomuneComponent,
    ModificaclienteComponent,
    ModificafatturaComponent,
    ModificacomuneComponent,
    AddprovinciaComponent,
    ModificaprovinciaComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule
  ],
  providers: [
    AppRoutingModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass:  AuthInterceptorsInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
